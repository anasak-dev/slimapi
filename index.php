<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '/vendor/autoload.php';

function newCommit(){
return "commit by admin anas";
}
function newCommit3(){
return "commit by admin anas";
}
function newCommit4(){
return "commit by anasak-dev";
}

function getDB()
{
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpass = "";
    $dbname = "webapi";
 
    $mysql_conn_string = "mysql:host=$dbhost;dbname=$dbname";
    $dbConnection = new PDO($mysql_conn_string, $dbuser, $dbpass); 
    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbConnection;
}


$app = new \Slim\App;


function newCommit2(){
return "commit by remote anas";
}
function newCommit5(){
return "commit by remote anasak-dev2";
}

function byAdmin2(){
return "commit by anasak-dev";
}
 

$app->get('/', function () {
    echo "Welcome to slim app";
});
$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});


$app->get('/getScore/{id}', function (Request $request,Response $response) use ($app) {
//   $app = \Slim\Slim::getInstance();
function admincommit(){
}

    try 
    {
        $db = getDB();
           $id = $request->getAttribute('id');
        $sth = $db->prepare("SELECT * 
            FROM `students`
             where `student_id` =:id");
 
 
        $sth->bindValue(':id',$id, PDO::PARAM_INT);
      $sth->execute();
 
   $student = $sth->fetch(PDO::FETCH_OBJ);
//   $response->getBody()->write("Hello, $id");

  //  return $response;
// var_dump($student);
  // while($row = $sth->fetch()) {
  //           echo $row['first_name']."<br/>";
  //       }
// $status = $response->withStatus();
// echo $status;
         if($student) {
            $response->withStatus(200);

             $response->getBody()->write(json_encode($student));
    $newResponse = $response->withHeader(
        'Content-type',
        'application/json; charset=utf-8'
    );
           // $response->headers->set('Content-Type', 'application/json');
           // 	echo json_encode($student);
		   
		   // $response->getBody()->write("Hello, $id");
           
            
            $db = null;
        return $newResponse;
        }
        
       else {
            throw new PDOException('No records found.');
        }
 
    } catch(PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
});


$app->post('/updateScore', function(Request $request,Response $response) use($app) {
 
    $allPostVars = $request->getParsedBody();
    $score = $allPostVars['score'];
    $id = $allPostVars['id'];
 
    try 
    {
        $db = getDB();
 
        $sth = $db->prepare("UPDATE students 
            SET score = :score 
            WHERE student_id = :id");
 
        $sth->bindParam(':score', $score, PDO::PARAM_INT);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->execute();


 
        $response->withStatus(200);
            $newResponse = $response->withHeader(
        'Content-type',
        'application/json; charset=utf-8'
    );
        echo json_encode(array("status" => "success", "code" => 1));
        $db = null;
 
    } catch(PDOException $e) {
        $app->response()->setStatus(404);
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
 
}); 
 

$app->post('/api', function () {                            
        echo 'post!';
});
$app->post('/insert', function () {                            
       $app->render('insert.html');
});

$app->run();